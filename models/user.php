<?php
class UserModel extends Model{
    public function register(){
         //Sanitize POST
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        $password = md5($post['password']);

        if($post['submit']){
            //Validation of fields 
                       
            if($post['name'] == '' || $post['password'] == '' || $post['email'] == '' || $post['age'] == ''){
                Messages::setMsg('Fill In All Fields Please', 'error');
                return;
            }
            
           //Insert into MyQSL user data           
           
            $this->query("INSERT INTO user(user_name, user_password, user_email, age) VALUES(:name, :password, :email, :age)");
                       
            $this->bind(':name', $post['name']);
            $this->bind(':password', $password); 
            $this->bind(':email', $post['email']);
            $this->bind(':age', $post['age']);           
           
            $this->execute();

            $this->query("INSERT INTO user_address(user_id, city, post_code, address) VALUES(:user_id, :city, :post_code, :address)");
            $this->bind(':city', $post['city']);
            $this->bind(':post_code', $post['post_code']);
            $this->bind(':address', $post['address']);
            $this->bind(':user_id', $this->lastInsertId());

            $this->execute();

            //Verify
            if($this->lastInsertId()){
                //Redirect
                header('Location: '.ROOT_URL.'users/login');
            }
        }
        return;
    }

    public function login(){
       //Sanitize POST
        $post = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

        $password = md5($post['password']);

        if($post['submit']){
           //Test if it is working
           // die("Submitted");
            var_dump($post['email']." pass= ".$password );
            

            //Compare login
            $this->query('SELECT * FROM user WHERE user_email = :email AND password = :password');
            $this->bind(':email', $post['email']);
            $this->bind(':password', $password);       
            
            $row = $this->single();

            if($row){
                $_SESSION['is_logged_in'] = true;
                $_SESSION['user_data'] = array(
                    "id" => $row['id'],
                    "name" => $row['name'],
                    "email" => $row['email']
                );
                 header('Location: '.ROOT_URL.'shares');
            } else {
                Messages::setMsg('Incorrect Login', 'error');   
            }            
        }
        return;
    }
}