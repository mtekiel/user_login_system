<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Register User</h3>
    </div>
    <div class="panel-body">        
        <form method="post" action="<?php $_SERVER['PHP_SELF']; ?>">
            <div class="form-group">
                <label>User Name</label>
                <input type="text" name="name" class="form-control" />
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="text" name="email" class="form-control"></input>
            </div>
            <div class="form-group">
                <label>Password</label>
                <input type="password" name="password" class="form-control" />
            </div>
             <div class="form-group">
                <label>Age</label>
                <input type="text" name="age" class="form-control" />
            </div>
            <div class="form-group">
                <label>City</label>
                <input type="text" name="city" class="form-control" />
            </div>
            <div class="form-group">
                <label>Post Code</label>
                <input type="text" name="post_code" class="form-control" />
            </div>
            <div class="form-group">
                <label>Address</label>
                <input type="text" name="address" class="form-control" />
            </div>
            <input type="submit" class="btn btn-primary" name= "submit" value="Submit" />
            
        </form>
    </div>
</div>

