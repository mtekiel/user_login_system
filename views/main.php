<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <title>User Login System</title>
</head>
<body>
    <nav class="navbar navbar-default">
        <div class="container">
           

            <a class="navbar-brand" href="#">User Login System Menu:</a>
            
            <div class="collapse navbar-collapse" id="navbar">
                <ul class="nav navbar-nav">
                    <li>
                        <a class="nav-link" aria-current="page" href="<?php echo ROOT_URL; ?>">Home</a>
                    </li>
                    <li>
                        <a class="nav-link" href="<?php echo ROOT_URL; ?>shares">Shares</a>
                    </li>
                    
                </ul>
                <ul class="nav navbar-nav navbar-right ">
                    <?php if(isset($_SESSION['is_logged_in'])) : ?>
                        <li><a href="<?php echo ROOT_URL; ?>">Welcome <?php echo $_SESSION['user_data']['name']; ?></a></li>
                        <li><a class="nav-link" href="<?php echo ROOT_URL; ?>users/logout">Logout</a></li>
                    <?php else : ?>
                        <li><a href="<?php echo ROOT_URL; ?>users/login">Login</a></li>
                        <li><a class="nav-link" href="<?php echo ROOT_URL; ?>users/register">Register</a></li>
                    <?php endif; ?>
                </ul>
                
            </div>
        </div>
    </nav>

<main class="container">
    
    <br><br><br>
    <div class="row" >  
        <?php Messages::display(); ?>                
        <?php require($view); ?>    
    </div>
  

</main><!-- /.container -->
</body>
</html>